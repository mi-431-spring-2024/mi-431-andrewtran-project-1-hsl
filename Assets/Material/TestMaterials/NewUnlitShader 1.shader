Shader "Unlit/TestShader#2"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _color ("Color", Color) = (0,0,0,0)
        _Distance ("Distance", Range(0,5)) = 2.5
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _color;
            float _Distance;

            v2f vert (appdata v)
            {
                v2f o;
                v.vertex.y = cos(v.vertex.y * _Distance);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.a = _color;
                col.r = _color;
                col.g = _color;
                col.b = _color;
                return col;
            }
            ENDCG
        }
    }
}
