Shader "Unlit/TestShader#1"
{
    Properties
    {
        _MainTex ("ProjectTexture", 2D) = "white" {}
        _TintColor ("TintColor", Color) = (0,0,0,0)
        _Transparency ("Transparency", Range(0.0,0.5)) = 0.25
        _Cutout ("CutoutThreshold", Range(0,0.2)) = 0.1
        _Distance ("Distance", float) = 0.5
        _Speed ("Speed", float) = 0.5
        _Amount ("Amount", float) = 0.5

    }
    SubShader
    {
        Tags {"Queue"="Transparent" "RenderType"="Opaque" }
        LOD 100

        ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            // Add properties here after declaring in "Properties"
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _TintColor;
            float _Transparency;
            float _Cutout;
            float _Distance;
            float _Speed;
            float _Amount;

            v2f vert (appdata v)
            {
                v2f o;
                // Adjusts the vertex x coordinates with the sine function
                v.vertex.x += sin(v.vertex.y * _Speed * _Distance * _Amount);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // Takes off red by amount
                clip(col.r - _Cutout);
                // Takes the alpha value for transparency
                col.a = _Transparency;
                return col;
            }
            ENDCG
        }
    }
}
