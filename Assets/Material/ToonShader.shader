Shader "Unlit/ToonShader"
{
    Properties
    {
        // Properties to adjust on the material component
        _Albedo ("Albedo", Color) = (1,1,1,1)
        _Shades ("Shades", Range(1,20)) = 4
        _InkColor ("InkColor", Color) = (0,0,0,0)
        _InkSize ("InkSize", Range(0,4)) = 0
    }
    // Renders the toon shader
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 150

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 worldNormal : TEXCOORD0;
               
            };

            // Define properties after declaring them
            float4 _Albedo;
            float _Shades;

            // Function to modify vertices of shader
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);               
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            // Function to make the shader produce color and amount of shading
            fixed4 frag (v2f i) : SV_Target
            {
                float cosineAngle = dot(normalize(i.worldNormal),normalize(_WorldSpaceLightPos0.xyz));
                cosineAngle = max(cosineAngle,0.0);
                // return fixed4(cosineAngle,cosineAngle,cosineAngle,1);
                cosineAngle = floor(cosineAngle * _Shades) / _Shades;
                return _Albedo * cosineAngle;
            }
            ENDCG
        }
        Pass
        {
            // Renders the outline
            // Renders in front
            Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            // Properties to change the outline color and size
            float4 _InkColor;
            float _InkSize;

            // Adjusts the outlines vertices
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex + _InkSize * v.normal);               
                return o;
            }
            // Renders the color on what we choose
            fixed4 frag (v2f i) : SV_Target
            {
               return _InkColor;
            }
            ENDCG
        }
    }
    // Provides shadows to the object
    Fallback "VertexLit"
}
